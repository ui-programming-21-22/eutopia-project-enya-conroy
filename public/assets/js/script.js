const canvas = document.getElementById("the_canvas")
const context = canvas.getContext("2d");

let username = localStorage.getItem('username');

const scale = .5;
const width = 240;
const height = 296;
const scaledWidth = scale * width;
const scaledHeight = scale * height;
const walkLoop = [0, 1, 2, 3];
const frameLimit = 7;

const cogWidth = 150;
const cogHeight = 200;

let score = 0;

let currentLoopIndex = 0;
let frameCount = 0;
let currentDirection = 0;
let speed = 3;
let scoreCount = 0;

if(score)
{
    scoreCount = score;
}

let randomX = Math.abs(Math.floor(Math.random() * 1099) - 50);
let randomY = Math.abs(Math.floor(Math.random() * 499) - 50);
let randomCogX = Math.abs(Math.floor(Math.random() * 7));
let randomCogY = Math.abs(Math.floor(Math.random() * 4));

function randoPos(rangeX, rangeY, delta){
    this.x = Math.abs(Math.floor(Math.random() * rangeX) - delta);
    this.y = Math.abs(Math.floor(Math.random() * rangeY) - delta);
}

function addName() {
    let header = document.getElementById("main-header");
    header.innerHTML = "Hello " + username;
}

addName();

let character = new Image();
character.src = "assets/media/robot.png";

let cogSprite = new Image();
cogSprite.src = "assets/media/head.png";

// GameObject holds positional information
// Can be used to hold other information based on requirements
function GameObject(spritesheet, x, y, width, height) {
    this.spritesheet = spritesheet;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.mvmtDirection = "None";
}

// Default Player
let player = new GameObject(character, 0, 0, 200, 200);
let cog = new GameObject(cogSprite, randomX, randomY, 100, 100);

// The GamerInput is an Object that holds the Current
// GamerInput (Left, Right, Up, Down, MouseClicks)
function GamerInput(input) {
    this.action = input; // Hold the current input as a string
}

// Default GamerInput is set to None
let gamerInput = new GamerInput("None"); //No Input

function clickableDpadReleased() {
    console.log(event);
}
function clickDpadYellow(){
    console.log(event);

}
function clickDpadBlue(){
    console.log(event);
}
function clickDpadRed(){
    console.log(event);
}
function clickDpadGreen(){
    console.log(event);
}
let yellowButton = document.getElementsByClassName("yellow")[0];
let blueButton = document.getElementsByClassName("blue")[0];
let redButton = document.getElementsByClassName("red")[0];
let greenButton = document.getElementsByClassName("green")[0];

function checkScore()
{
 if(scoreCount == 0) {
    cogSprite.src = "assets/media/head.png";
 }
 if(scoreCount == 1) {
    cogSprite.src = "assets/media/arm1.png";
 }
 if(scoreCount == 2) {
    cogSprite.src = "assets/media/body.png";
 }
 if(scoreCount == 3) {
    cogSprite.src = "assets/media/arm2.png";
 } if(scoreCount == 4) {
    cogSprite.src = "assets/media/wheel.png";
 }

}


function input(event) {
    // Take Input from the Player
    // console.log("Input");
    // console.log("Event type: " + event.type);
    // console.log("Keycode: " + event.keyCode);

    if (event.type === "keydown") {
        switch (event.keyCode) {
            case 37: // Left Arrow
                gamerInput = new GamerInput("Left");
                break; //Left key
            case 38: // Up Arrow
                gamerInput = new GamerInput("Up");
                break; //Up key
            case 39: // Right Arrow
                gamerInput = new GamerInput("Right");
                break; //Right key
            case 40: // Down Arrow
                gamerInput = new GamerInput("Down");
                break; //Down key
            case 173: //- 
                gamerInput = new GamerInput("-");
                break;   
            case 61: //=
                gamerInput = new GamerInput("=");  
                break;
            case 32:
                gamerInput = new GamerInput("Space");
                break; //Space key    
            default:
                gamerInput = new GamerInput("None"); //No Input
        }
    } else {
        gamerInput = new GamerInput("None");
    }
}

// Spritesheet atlas references
// row 0 left
// row 1 right
// row 2 down
// row 3 up


function update() {
    // console.log("Update");
    // Check Input
    if (gamerInput.action === "Up") {
        //console.log("Move Up");
        player.y -= speed; // Move Player Up
        currentDirection = 3;
    } else if (gamerInput.action === "Down") {
        //console.log("Move Down");
        player.y += speed; // Move Player Down
        currentDirection = 2;
    } else if (gamerInput.action === "Left") {
        //console.log("Move Left");
        player.x -= speed; // Move Player Left
        currentDirection = 0;
    } else if (gamerInput.action === "Right") {
        //console.log("Move Right");
        player.x += speed; // Move Player Right
        currentDirection = 1;
    } else if (gamerInput.action === "None") {
        //console.log("player no longer moving")    
    } else if (gamerInput.action === "=") {
        console.log("=");
        speed++;
    }
    else if (gamerInput.action === "-") {
        console.log("-");
        speed--;
    }
    else if (gamerInput.action === "Space") {
    console.log("Space Bar Pressed");
    playSound();
    }
}

function playSound()
{
  let music = new Audio("assets/media/music.mp3");
  music.play();
}

function drawFrame(image, frameX, frameY, canvasX, canvasY) {
    context.drawImage(image,
                  frameX * width, frameY * height, width, height,
                  canvasX, canvasY, scaledWidth, scaledHeight);
}

function animate() {
    if (gamerInput.action != "None"){
        frameCount++;
        if (frameCount >= frameLimit) {
            frameCount = 0;
            currentLoopIndex++;
            if (currentLoopIndex >= walkLoop.length) {
                currentLoopIndex = 0;
            }
        }      
    }
    else{
        currentLoopIndex = 0;
    }
    drawFrame(player.spritesheet, walkLoop[currentLoopIndex], currentDirection, player.x, player.y);
}

cogPosition = new randoPos(1099, 499, 50);


function manageCog(){
    // place the cog
    context.drawImage(cogSprite, randomCogX, randomCogY, 240, 608, cogPosition.x, cogPosition.y, cogWidth, cogHeight);

    // check for collision 
    if(player.x <= (cogPosition.x + 70) && (player.x + 100) >= cogPosition.x && player.y <= (cogPosition.y + 90) && (player.y + 120) >= cogPosition.y)
    {
        let music = new Audio("assets/media/metal.wav");
        music.play("metal.wav");
        console.log(" collision !");
        scoreCount = scoreCount += 1;
        cogPosition.x = (Math.floor(Math.random() * 1000));
        cogPosition.y = (Math.floor(Math.random() * 400));
    }
  
}

function currentTime()
{
    const months = ["January", "February", 
    "March", "April", "May", "June", "July", "August", 
    "September", "October", "November", "December"]; 
    //array of months because otherwise the month will just output as a number

    let date = new Date();
    let hour = date.getHours();
    let min = date.getMinutes();
    let sec = date.getSeconds();
    let day = date.getDate();
    let month = months[date.getMonth()];
    let currentyear = date.getFullYear();
    let dayOrder = "th";
    let secNo = "";
    let minNo = "";
    let hourNo = "";

    switch(day)
    {
        case 1:
            dayOrder = "st";
            break;
        case 2:
            dayOrder = "nd";
            break;
        case 3:
            dayOrder = "rd";
            break;
        case 21:
            dayOrder = "st";
            break;
        case 22: 
            dayOrder = "nd";
            break;
        case 23: 
            dayOrder = "rd";
            break;
        case 31:
            dayOrder = "st";
            break;
        default:
            dayOrder = "th";
    }

    if (sec < 10)
    {
        secNo = "0";
    }
    if (min < 10)
    {
        minNo = "0";
    }
    if (hour < 10)
    {
        hourNo = "0";
    }

  document.getElementById("time").innerHTML = hourNo + hour + " : " + minNo + min + " : " + secNo + sec ;
  document.getElementById("date").innerHTML = day + dayOrder + " " + month + " " + currentyear;
}

var fillVal =0;

function drawTimer() {
    var width = 1100;
    var height = 20;
    var max = 1100;
    var val = 10;
  
    // Draw the background
    context.fillStyle = "#DCDCDC";
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.fillRect(0, 0, width, height);
  
    // Draw the fill
    context.fillStyle = "#696969";
    context.fillRect(0, 0, fillVal * width, height);
    fillVal  += 0.0005;

    console.log(fillVal);
}


var dynamic = nipplejs.create({
    color: 'white',
    zone: document.getElementById("joy")
});

dynamic.on('added', function (evt, nipple) {
    //nipple.on('start move end dir plain', function (evt) {
    nipple.on('dir:up', function (evt, data) {
       //console.log("direction up");
       gamerInput = new GamerInput("Up");
    });
    nipple.on('dir:down', function (evt, data) {
        //console.log("direction down");
        gamerInput = new GamerInput("Down");
     });
     nipple.on('dir:left', function (evt, data) {
        //console.log("direction left");
        gamerInput = new GamerInput("Left");
     });
     nipple.on('dir:right', function (evt, data) {
        //console.log("direction right");
        gamerInput = new GamerInput("Right");
     });
     nipple.on('end', function (evt, data) {
        //console.log("mvmt stopped");
        gamerInput = new GamerInput("None");
     });
  });

function checkLose(){
    if(fillVal >= 1){
        console.log("game over");
        alert("GAME OVER");
            document.location.reload();
            clearInterval(interval);
    }
}

function checkWin() {
    if(scoreCount == 5)
    {
        console.log("win");
        alert("CONGRATS YOU WIN");
        document.location.reload();
        clearInterval(interval); 
    }
}

function draw() {
    context.clearRect(0,0, canvas.width, canvas.height);
    drawTimer();
    manageCog();
    animate();
    writeScore();
}

function writeScore()
{
    let scoreString = "Pieces Collected: " + scoreCount + " / 5";
    context.font = "22px sans-serif";
    context.fillStyle = "white";
    context.fillText(scoreString, 860, 60);
}

function clickDpadY()
{
    gamerInput = new GamerInput("Up");
}
function clickDpadX()
{
    gamerInput = new GamerInput("Left");
}
function clickDpadB()
{
    gamerInput = new GamerInput("Right");
}
function clickDpadA()
{
    gamerInput = new GamerInput("Down");
}
function stopClick()
{
    gamerInput = new GamerInput("None");
}

function gameloop() {
    update();
    draw();
    currentTime();
    checkScore();
    checkLose();
    checkWin();
    window.requestAnimationFrame(gameloop);
}

// Handle Active Browser Tag Animation
window.requestAnimationFrame(gameloop);

// https://developer.mozilla.org/en-US/docs/Web/API/window/requestAnimationFrame

window.addEventListener('keydown', input);
// disable the second event listener if you want continuous movement
window.addEventListener('keyup', input);

// disable the second event listener if you want continuous movement
window.addEventListener('keyup', input);
